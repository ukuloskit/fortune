import subprocess

from flask import Flask

app = Flask(__name__)


@app.route('/')
def get_fortune():
    output = subprocess.check_output(["fortune", "-o", "fortunes"])
    return output.decode()


if __name__ == "__main__":
    app.run()
