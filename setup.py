# -*- coding: utf-8 -*-

from setuptools import setup


setup(
    name="fortune",
    version="0.0.1",
    packages=[
        "fortune",
    ],
    install_requires=[
        "Flask==0.12.2",
    ],
    author="Uku Loskit",
)
